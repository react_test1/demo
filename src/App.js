import ActionsBar from "./ActionsBar"
import DisplayList from "./DisplayList"

// NOTE: useState of lit
import { useState } from "react";


export default function App() {
    // funtion ควรทำในตัวแม่ดีกว่าตัวลูกเพราะ ตัวลูกไม่สามารถแก้ไขอะไรได้ถ้าเป็นตัวลูก
    // NOTE: new of input in list
    const [inputText, setInputText] = useState("");

    // NOTE: set data of list = []
    const [dataList, setDataList] = useState([]);

    const onInputChange = (event) => {
        // NOTE: Edit of input value
        setInputText(event.target.value)
    }

    const onAdd = () => {
        //NOTE: add ข้อมูลเก่าและเพิ่มข้อมูลใหม่เข้าไป
        setDataList([...dataList, inputText]);

        //NOTE: Reset
        setInputText("");
    }

    const onRemove = () => {
        // NOTE: delete of list ที่พึ่งเพิ่มเข้าไปใหม่ หรือล่าสุด
        setDataList(dataList.slice(0, -1));
    }


    return <>
        <ActionsBar value={inputText} onInputChange={onInputChange} onAddClick={onAdd} onRemoveClick={onRemove} />
        <DisplayList list={dataList} />
    </>
}