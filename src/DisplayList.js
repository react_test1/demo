export default function DisplayList({ list }) {

    const mapListToElement = () => {
        let id = 0;
        // NOTE: map คล้าย loop วนโชว์แสดงข้อมูล
        return list.map(element => {
            id++;
            return <li key={id}>{element}</li>
        })
    }
    // เรียกฟังก์ชัน มาแสดง
    return <ul>
        {mapListToElement()}
    </ul>
}