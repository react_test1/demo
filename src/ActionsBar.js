export default function ActionsBar({ value, onInputChange, onAddClick, onRemoveClick }) {

    return <div style={{ display: 'flex', flexDirection: 'colomn' }}>
        <input onChange={onInputChange} value={value}></input>
        <button onClick={onAddClick}>Add</button>
        <button onClick={onRemoveClick}>Remove</button>
    </div>
}